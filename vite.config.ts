import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

export default defineConfig({
  plugins: [svelte()],
  server: {
    open: true,
    hmr: {
      overlay: false,
    },
  },
  build: {
    sourcemap: true,
    brotliSize: false,
    rollupOptions: {
      output: {
        manualChunks: {},
      },
    },
    terserOptions: {
      compress: {
        pure_funcs: ['console.log', 'console.clear'],
      },
    },
  },
})
