module.exports = {
  env: {
    browser: true,
    node: true,
  },
  plugins: ['prettier'],
  extends: ['eslint:recommended', 'plugin:prettier/recommended'],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  overrides: [
    {
      files: ['*.svelte'],
      processor: 'svelte3/svelte3',
      plugins: ['svelte3', 'prettier'],
      extends: ['plugin:@typescript-eslint/recommended', 'prettier'],
      settings: {
        'svelte3/typescript': true,
      },
      rules: {
        '@typescript-eslint/no-empty-function': 0,
      },
    },
    {
      files: ['*.ts'],
      plugins: ['prettier'],
      extends: ['plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
      rules: {
        '@typescript-eslint/no-empty-function': 0,
      },
    },
  ],
  ignorePatterns: ['/public', '/build', '/dist'],
  rules: {
    'max-len': ['error', { code: 120, ignoreStrings: true, ignoreRegExpLiterals: true }],
    'no-console': 0,
    'no-unused-vars': [
      0,
      {
        argsIgnorePattern: '[_]',
        varsIgnorePattern: '[_]',
        ignoreRestSiblings: true,
      },
    ],
    'no-param-reassign': [2, { props: false }],
    'no-empty-function': ['error', { allow: ['arrowFunctions'] }],
    'operator-linebreak': ['error', 'before'],
  },
}
