/* eslint-disable */
function clamp(val, min, max) {
  return Math.max(min, Math.min(val, max));
}

function animate(params) {
  const { duration = 300, timing = (val) => val, cb = () => {}, reverseCb = () => {} } = params;
  let { draw = () => {}, paused = false, reversed = false } = params;
  let start = performance.now();
  let prevProgress = reversed ? 1 : 0;
  let currProgress = 0;
  let timeScale = 1;
  let id = null;

  function anim(time) {
    if (paused) return;
    const diff = Math.max(time - start, 1);
    const progress = (diff / duration) * timeScale;
    currProgress = reversed ? prevProgress - progress : prevProgress + progress;
    if (currProgress > 1 || currProgress < 0) {
      currProgress = currProgress > 1 ? 1 : 0;
    }
    const timeFraction = timing(currProgress);
    draw(timeFraction);
    if (currProgress === 1 || currProgress === 0) {
      stopAnim(true);
      currProgress === 1 ? cb(anim) : reverseCb(anim);
      return;
    }
    id = requestAnimationFrame(anim);
  }

  function stopAnim(isFinished) {
    paused = true;
    prevProgress = isFinished ? (reversed || timeScale < 0 ? 1 : 0) : currProgress;
    cancelAnimationFrame(id);
    return anim;
  }

  function startAnim(isPaused = false, skip = false) { 
    if (timeScale === 0) return anim;
    if (!skip && reversed && currProgress === 1 && timeScale < 0) prevProgress = 0;
    paused = isPaused;
    start = performance.now();
    id = requestAnimationFrame(anim);
    return anim;
  }

  anim.play = () => (paused ? startAnim() : anim);

  anim.pause = () => (!paused ? stopAnim() : anim);

  anim.toggle = () => (paused ? startAnim() : stopAnim());

  anim.restart = (func, isPaused) => {
    stopAnim();
    if (func) {
      draw = func;
    }
    timeScale = 1;
    reversed = params.reversed ?? false;
    prevProgress = reversed ? 1 : 0;
    return startAnim(isPaused ?? params.paused ?? false);
  };

  anim.paused = () => paused;

  anim.progress = (val) => {
    if (val === undefined) return currProgress;
    stopAnim();
    const clampedVal = clamp(val, 0, 1);
    prevProgress = reversed ? 1 - clampedVal : clampedVal;
    startAnim(false, true);
  }

  anim.reversed = () => (reversed && timeScale >= 0) || (!reversed && timeScale < 0);

  anim.reverse = () => {
    reversed = !reversed;
    stopAnim();
    return startAnim();
  };

  anim.timeScale = (val) => {
    if (val === undefined) return timeScale;
    timeScale = +val;
    if (timeScale === 0) {
      return stopAnim();
    } else if (!paused) {
      start = performance.now();
      prevProgress = currProgress;
      return anim;
    }
  };

  id = requestAnimationFrame(anim);

  return anim;
}

export default animate;