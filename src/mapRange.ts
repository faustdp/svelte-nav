export default function mapRange(val: number, inMin: number, inMax: number, outMin: number, outMax: number) {
  if (!Number.isFinite(val)) return 0
  if (val <= inMin || val >= inMax) return val <= inMin ? outMin : outMax
  const percent = (val - inMin) / (inMax - inMin)
  const result = (outMax - outMin) * percent + outMin
  return Number.isFinite(result) ? result : 0
}
