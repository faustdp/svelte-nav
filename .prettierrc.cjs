module.exports = {
  tabWidth: 2,
  printWidth: 120,
  singleQuote: true,
  useTabs: false,
  semi: false,
  endOfLine: 'lf',
  trailingComma: 'all',
  bracketSpacing: true,
}